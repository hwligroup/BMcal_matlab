%%% BM calculation and hisogram plotting
%%% by Wen-Hsuan Chang
%%% ver 1.0 (20140328)
%%% ver 1.1 (20140520) add x-y ratio criteria

%%% need function "BMana.m","fstuck.m","stuckcal.m"

clc;
clear all;

% input file
% int_file=1;
% fin_file=1;
valid=[1 2 3 ];
name='demo1';

%% BM report generation
%%% file generation 
%%% the 1st column is BM of x, 2nd is BM of y, 3rd is x-y ratio

%for i=int_file:1:fin_file;
for i=valid;
    title_f=['PRE' num2str(i) '.csv'];
    BM_report=BMana(title_f);
    csvwrite(['aftsummary_' num2str(i) '.csv'], BM_report);
end

%% read data
BM_pool=zeros(0);
%for i=int_file:1:fin_file;
for i=valid;
    title_file2=['presummary_' num2str(i) '.csv'];
    BM_data2=csvread(title_file2);
    title_file=['aftsummary_' num2str(i) '.csv'];
    BM_data=csvread(title_file);
    num_aoi=size(BM_data,1);
    % find the tether that is single tether and well fitted(# of unfit data
    % is less than 10 %)
    index=find(BM_data2(:,3)>0.8 & BM_data2(:,3)<1.2 );
    %csvwrite(['useful data_' num2str(i) '.csv'], index);
    %get the data that fulfill the x-y ratio criteria
    get_data=BM_data(index,1);
    %get_data=BM_data(:,1);
    BM_pool=[BM_pool,get_data'];
end

%% histogram plotting
BM_pool=BM_pool.*72.5;
hist(BM_pool,[2.5:5:200]);
axis tight;
xlim([0 100]);
%ylim([0 80]);
title([' N=' num2str(size(BM_pool,2))],'FontSize',18);
xlabel('B.M.(nm)','FontSize',18);
ylabel('count','FontSize',18);
saveas(gcf,[name '.tiff'])



