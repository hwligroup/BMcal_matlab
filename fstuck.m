function ref=fstuck(datax,datay,num_aoi,nF);
%%% find the stuck bead

r1=0;
r2=0;
diff=inf;
%curr_diff=zeros(num_aoi);

for a1=1:1:(num_aoi-1);
    %%% find the well-fitted bead(we cannot use unfitted data)
    if nF(a1)<5;
        for a2=(a1+1):1:num_aoi;
            curr_diff=sum( ( datax(:,a1)-datax(:,a2)).^2) + sum( ( datay(:,a1)- datay(:,a2)).^2);
            %%% find the well-fitted bead(we cannot use unfitted data) 
            if nF(a2)<5;
                %%% find the least movable beads as stuck
                if  curr_diff<diff;
                    r1=a1;
                    r2=a2;
                    diff=curr_diff;
                end
            end
        end
    end
end

ref=[r1,r2]



     
