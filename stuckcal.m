function data=stuckcal(raw,r1,r2,num_aoi)
%raw=raw data
%r1,r2 are reference beads,
% making calculation matrix
m_dif=diff(raw);
cal_r=repmat((m_dif(:,r1)+m_dif(:,r2))./2,1,num_aoi);
%cal_r=repmat(m_dif(:,r1),1,num_aoi);
cal_rr=[zeros(1,num_aoi);cal_r];
cal=cumsum(cal_rr);
%m_cal=[zeros(1,num_aoi);cal];

% data caliberation

data=raw-cal;
end