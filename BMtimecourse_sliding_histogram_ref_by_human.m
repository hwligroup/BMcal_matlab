%%% BM calculation and timecourse plotting(sliding window)
%%% by Wen-Hsuan Chang
%%% ver 1.0 (20140328)
%%% ver 1.5 (20150320) 

%%% need function: BMtc Bmana fstuck stuckcal
clc;
clear all;
pre_file='PRE1.csv';
aft_file='AFT1.csv';
%aft_file2='tc_aft2.csv';
%aft_file3='tc_aft3.csv';
filename='20170214 DMC1- AC263 -1.3uM ';
%%% the space in the time course btwn pre and aft
space=20;
%%% pre file is the file that is before protein adding
%%% aft file is the file that is after protein adding

winsize=20; %%% at least "20"


%% calculation_sliding
[x_tc_p,time_p]=BMtc(pre_file,winsize);
[x_tc_a,time_a]=BMtc(aft_file,winsize);
%[x_tc_a2,time_a2]=BMtc(aft_file2,winsize);
%[x_tc_a3,time_a3]=BMtc(aft_file3,winsize);

%time_total=[[time_p - max(time_p)-space] ; [time_a]; [[time_a2]+max(time_a)]];
%trace_total=[x_tc_p; x_tc_a; x_tc_a2];
%trace_aft=[x_tc_a; x_tc_a2; x_tc_a3];
time_total=[[time_p-max(time_p)-space];time_a];
trace_total=[x_tc_p;x_tc_a];
trace_total=trace_total.*72.5;
%%% cut off B.M. since the data that are not fitted can interfere the trace
trace_total(trace_total>160)=NaN;
% 
output=[time_total trace_total];

csvwrite(['sliding_' filename '_timecourse.csv'],output);

%% filtrated data_sliding

BM_data_p=BMana(pre_file);
csvwrite(['sliding_pre_' filename '_BManalysis.csv'], BM_data_p);

BM_data_a=BMana(aft_file);
csvwrite(['sliding_aft_' filename '_BManalysis.csv'], BM_data_a);

%BM_data_a=BMana(aft_file2);
%csvwrite(['aft2_' filename '_BManalysis.csv'], BM_data_a);

%BM_data_a=BMana(aft_file3);
%csvwrite(['aft3_' filename '_BManalysis.csv'], BM_data_a);
index=find(BM_data_p(:,3)>0.8 & BM_data_p(:,3)<1.2 & BM_data_a(:,4)< 10 );
index_q= find(BM_data_p(:,3)>0.8 & BM_data_p(:,3)<1.2 & BM_data_a(:,4)>10 );



%% plot timecourse_sliding
    
for i=index'
    tc_plot=plot(time_total,trace_total(:,i),'b.');
    title(['tether#' num2str(i) '_timecourse']);
    xlim([[0 - max(time_p)-space] max(time_total)]);
    ylim([0 150]);
    xlabel('time(second)');
    ylabel('BM(nm)');
    saveas(tc_plot,[ 'sliding_' num2str(i) '_timecourse.tiff']);
end

for i=index_q'
    tc_plot=plot(time_total,trace_total(:,i),'b.');
    title(['tether#' num2str(i) '_timecourse']);
    xlim([[0 - max(time_p)-space] max(time_total)]);
    ylim([0 150]);
    xlabel('time(second)');
    ylabel('BM(nm)');
    saveas(tc_plot,[ 'sliding_NaN too much_' num2str(i) '_timecourse.tiff']);
end

%% histogram plot
pool_his=zeros(0);
for j=index'
    get_his=x_tc_a(:,j);
    pool_his=[pool_his; get_his];
end
 pool_his= pool_his.*72.5;
 pool_his(pool_his>160)=NaN;

hist(pool_his,[0.5:1:160]);
axis tight;
xlim([0 160]);
%ylim([0 80]);
title(filename,'FontSize',18);
xlabel('B.M.(nm)','FontSize',18);
ylabel('count','FontSize',18);
saveas(gcf,['sliding_his_' filename '.tiff'])

