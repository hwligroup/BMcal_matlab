function BMdata=BMana_ref(filename,filename_s);

%%% function of BM analysis
%%% by Wen-Hsuan Chang
%%% ver 1.0 (20140328)


%% read and refrom data
raw=csvread(filename,1,0);
raw_ref=csvread(filename_s,1,0);

%% reform the raw data
num_aoi=raw(size(raw,1),2);
num_frame=(raw(size(raw,1),1)-raw(1,1)+1);
x=zeros(num_frame,num_aoi);
y=zeros(num_frame,num_aoi);
%x_position_raw_abs=zeros(num_frame,num_aoi);
%y_position_raw_abs=zeros(num_frame,num_aoi);

for i=1:1:num_aoi;
    for j=0:1:(num_frame-1);
    x(j+1,i)=raw(i+(j.*num_aoi),9);
    end
end

for i=1:1:num_aoi;
    for j=0:1:(num_frame-1);
    y(j+1,i)=raw(i+(j.*num_aoi),10);
    end
end

%% reform the raw stuck data
num_aoi_s=raw_ref(size(raw_ref,1),2);
num_frame_s=(raw_ref(size(raw_ref,1),1)-raw_ref(1,1)+1);
xs=zeros(num_frame_s,num_aoi_s);
ys=zeros(num_frame_s,num_aoi_s);
%x_position_raw_abs=zeros(num_frame,num_aoi);
%y_position_raw_abs=zeros(num_frame,num_aoi);

for i=1:1:num_aoi_s;
    for j=0:1:(num_frame_s-1);
    xs(j+1,i)=raw_ref(i+(j.*num_aoi_s),9);
    end
end

for i=1:1:num_aoi_s;
    for j=0:1:(num_frame_s-1);
    ys(j+1,i)=raw_ref(i+(j.*num_aoi_s),10);
    end
end

xd=diff(xs);
yd=diff(ys);

%% find the percentage of no fit
nF = ( sum(x==0,1)./num_frame.*100 );

%% normalization
%data raw
x=x-repmat(mean(x,1),num_frame,1);
y=y-repmat(mean(y,1),num_frame,1);
%stuck raw
xs=xs-repmat(mean(xs,1),num_frame,1);
ys=ys-repmat(mean(ys,1),num_frame,1);

%% find reference bead to caliberate stage drift
ref=fstuck(xd,yd,num_aoi_s,nF);

%% caliberate stage drift
r1=ref(1);
r2=ref(2);
x=stuckcal_r(x,xs,r1,r2,num_aoi);
y=stuckcal_r(y,ys,r1,r2,num_aoi);

% matrix for debug
%x_cal=stuckcal(x,xd,r1,r2,num_aoi);
%y_cal=stuckcal(y,yd,r1,r2,num_aoi);

%% calculare B.M. and x-y ratio
%std of x, std of y, x-y ratio
mean_BM=zeros(num_aoi,3);
for i=1:1:num_aoi;
    mean_BM(i,1)=std(x(:,i));
end

for i=1:1:num_aoi;
    mean_BM(i,2)=std(y(:,i));
end

for i=1:1:num_aoi;
    mean_BM(i,3)=mean_BM(i,1)/mean_BM(i,2);
end

BMdata=[mean_BM nF'];

end

