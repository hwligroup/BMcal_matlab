function y = boots(x, B)
% x = The original sample
% B = number of bootstrap samples
n=length(x);
    if nargin<2 % number of input arguments
    B=1;
    end
y=zeros(n,B);
    for j=1:B % each column contains one bootstrap sample
        for i=1:n
        k=floor(1+n*rand);
        y(i , j)=x(k);
        end
    end