%%% BM calculation and timecourse plotting(fixed window)
%%% by Wen-Hsuan Chang
%%% ver 1.0 (20140328)
%%% ver 1.5 (20150320) 


%%%%% TIME scale is not right!!!!!


%%% need function: 'BMtc_fixed.m"
clc;
clear all;
pre_file='1_pre.csv';
aft_file='1_aft.csv';
filename='(fixed)20150319 yDmc1 2uM';
% pre file is the file that is before protein adding
% aft file is the file that is after protein adding

winsize=20; %%% at least "20"

% %% chosse tether to plot
% predata=csvread('presummary_1.csv');
% index=(find(predata(:,3)>0.8&predata(:,3)<1.2));
% valid=index';

%% calculation
[x_tc_p,time_p]=BMtc_fixed(pre_file,winsize);
[x_tc_a,time_a]=BMtc_fixed(aft_file,winsize);


time_total=[[time_p - max(time_p)-100] ; [time_a]];
trace_total=[x_tc_p ; x_tc_a];
trace_total=trace_total.*72.5;
% cut off B.M. since the data that are not fitted can interfere the trace
trace_total(trace_total>160)=NaN;

output=[time_total trace_total];

csvwrite([filename '_timecourse.csv'],output);

%% filtrated data

BM_data_p=BMana(pre_file);
csvwrite(['pre_' filename '_BManalysis.csv'], BM_data_p);

BM_data_a=BMana(aft_file);
csvwrite(['aft_' filename '_BManalysis.csv'], BM_data_a);

index=find(BM_data_p(:,3)>0.8 & BM_data_p(:,3)<1.2 & BM_data_a(:,4)< 10 );

% %% calculation (multiple file)
% for i=1:3;
%     filename=[num2str(i) '.csv'];
%     [x_tc,time]=BMtc(filename,winsize);
%     x_tc_i=x_tc;
%     time_i=time;
%     csvwrite([num2str(i) '_timecourse.csv'],x_tc);
%     csvwrite([num2str(i) '_time.csv'],time);
% end

% %% generate pool data
% M_1=csvread('1_timecourse.csv');
% M_2=csvread('2_timecourse.csv');
% M_3=csvread('3_timecourse.csv');
% t_1=csvread('1_time.csv');
% t_2=csvread('2_time.csv');
% t_3=csvread('3_time.csv');
% time_pool=[t_1; t_2;t_3];
% %time_pool=time_pool.*0.33;
% BM_pool=[M_1; M_2; M_3];
% BM_pool=BM_pool.*72.5;


%% plot timecourse
%     % read predata
%     title_file_pre=['presummary_1.csv'];
%     BM_data_pre=csvread(title_file_pre);
%     num_aoi=size(BM_data_pre,1);
%     % find the tether that is single tether
%     index=(find(BM_data_pre(:,3)>0.8&BM_data_pre(:,3)<1.2));
%     x_tc=x_tc.*72.5;
%     %valid=[1 2 7 8];
    
for i=index';
    tc_plot=plot(time_total,trace_total(:,i),'b.');
    title(['tether#' num2str(i) '_timecourse']);
    xlim([[0 - max(time_p)-100] max(time_total)]);
    ylim([0 150]);
    xlabel('time(second)');
    ylabel('BM(nm)');
    saveas(tc_plot,[ 'fixed_' num2str(i) '_timecourse.tiff']);
end
% for i=index';
%     tc_plot=plot(time_total,trace_total(:,i),'b.');
%     title(['tether#' num2str(i) '_timecourse']);
%     xlim([[0 - max(time_p)-100] max(time_total)]);
%     ylim([0 500]);
%     xlabel('time(second)');
%     ylabel('BM(nm)');
%     saveas(tc_plot,[ 'nan500_' num2str(i) '_timecourse.tiff']);
% end

% 
% time_total=[time_p ; [time_a + max(time_p)]];
% trace_total=[x_tc_p ; x_tc_a];
% trace_total=trace_total.*72.5;
% 
% output=[time_total trace_total];
% 
% csvwrite([filename '_timecourse.csv'],output);
% 
% %% filtrated data
% 
% BM_data_p=BMana(pre_file);
% csvwrite(['pre_' filename '_BManalysis.csv'], BM_data_p);
% 
% BM_data_a=BMana(aft_file);
% csvwrite(['aft_' filename '_BManalysis.csv'], BM_data_a);
% 
% index=find(BM_data_p(:,3)>0.8 & BM_data_p(:,3)<1.2 );
% 
% % %% calculation (multiple file)
% % for i=1:3;
% %     filename=[num2str(i) '.csv'];
% %     [x_tc,time]=BMtc(filename,winsize);
% %     x_tc_i=x_tc;
% %     time_i=time;
% %     csvwrite([num2str(i) '_timecourse.csv'],x_tc);
% %     csvwrite([num2str(i) '_time.csv'],time);
% % end
% 
% % %% generate pool data
% % M_1=csvread('1_timecourse.csv');
% % M_2=csvread('2_timecourse.csv');
% % M_3=csvread('3_timecourse.csv');
% % t_1=csvread('1_time.csv');
% % t_2=csvread('2_time.csv');
% % t_3=csvread('3_time.csv');
% % time_pool=[t_1; t_2;t_3];
% % %time_pool=time_pool.*0.33;
% % BM_pool=[M_1; M_2; M_3];
% % BM_pool=BM_pool.*72.5;
% 
% 
% %% plot timecourse
% %     % read predata
% %     title_file_pre=['presummary_1.csv'];
% %     BM_data_pre=csvread(title_file_pre);
% %     num_aoi=size(BM_data_pre,1);
% %     % find the tether that is single tether
% %     index=(find(BM_data_pre(:,3)>0.8&BM_data_pre(:,3)<1.2));
% %     x_tc=x_tc.*72.5;
% %     %valid=[1 2 7 8];
%    
%  for i=index';
%     tc_plot=plot(time_total,trace_total(:,i),'b.');
%     title(['tether#' num2str(i) '_timecourse']);
%     xlim([[0 - max(time_p)-100] max(time_total)]);
%     ylim([0 150]);
%     xlabel('time(second)');
%     ylabel('BM(nm)');
%     saveas(tc_plot,[ 'fixed_' num2str(i) '_timecourse.tiff']);
% end
% for i=index';
%     tc_plot=plot(time_total,trace_total(:,i),'b.');
%     title(['tether#' num2str(i) '_timecourse']);
%     xlim([[0 - max(time_p)-100] max(time_total)]);
%     ylim([0 500]);
%     xlabel('time(second)');
%     ylabel('BM(nm)');
%     saveas(tc_plot,[ 'fixed_500_' num2str(i) '_timecourse.tiff']);
% end