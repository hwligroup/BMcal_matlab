function BMdata=BMana(filename);

%%% function of BM analysis
%%% by Wen-Hsuan Chang
%%% ver 1.0 (20140328)


%% read and refrom data
raw=csvread(filename,1,0);

%% reform the raw data
num_aoi=(raw(size(raw,1),2));
% num_aoi=1000;
num_frame=(raw(size(raw,1),1)-raw(1,1)+1);
x=zeros(num_frame,num_aoi);
y=zeros(num_frame,num_aoi);
%x_position_raw_abs=zeros(num_frame,num_aoi);
%y_position_raw_abs=zeros(num_frame,num_aoi);

for i=1:1:num_aoi;
    for j=0:1:(num_frame-1);
    x(j+1,i)=raw(i+(j.*num_aoi),6);
    end
end

for i=1:1:num_aoi;
    for j=0:1:(num_frame-1);
    y(j+1,i)=raw(i+(j.*num_aoi),7);
    end
end

xd=diff(x);
yd=diff(y);

%% find the percentage of no fit
nF = ( sum(x==0,1)./num_frame.*100 );

%% normalization
x=x-repmat(mean(x,1),num_frame,1);
y=y-repmat(mean(y,1),num_frame,1);


%% find reference bead to caliberate stage drift
ref=fstuck(xd,yd,num_aoi,nF);

%% caliberate stage drift
r1=ref(1);
r2=ref(2);

x=stuckcal(x,r1,r2,num_aoi);
y=stuckcal(y,r1,r2,num_aoi);

% matrix for debug
%x_cal=stuckcal(x,xd,r1,r2,num_aoi);
%y_cal=stuckcal(y,yd,r1,r2,num_aoi);

%% calculare B.M. and x-y ratio
%std of x, std of y, x-y ratio
mean_BM=zeros(num_aoi,3);
for i=1:1:num_aoi;
    mean_BM(i,1)=std(x(:,i));
end

for i=1:1:num_aoi;
    mean_BM(i,2)=std(y(:,i));
end

for i=1:1:num_aoi;
    mean_BM(i,3)=mean_BM(i,1)/mean_BM(i,2);
end

BMdata=[mean_BM nF'];

end

